# cas-blueprints

Cloud Assembly Blueprints:

OpenCart :
  - Two tier shopping cart application that is cloud agnostic. Requires Ubuntu image mapping.

OpenCart with LoadBalancer :
  - Two tier shopping cart application with LoadBalancer that is cloud agnostic. Requires Ubuntu image mapping.
